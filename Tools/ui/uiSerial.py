# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'serial.ui'
#
# Created: Wed Apr 29 05:44:21 2015
#      by: PyQt4 UI code generator 4.9.6
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName(_fromUtf8("MainWindow"))
        MainWindow.resize(327, 495)
        self.centralwidget = QtGui.QWidget(MainWindow)
        self.centralwidget.setObjectName(_fromUtf8("centralwidget"))
        self.label_3 = QtGui.QLabel(self.centralwidget)
        self.label_3.setGeometry(QtCore.QRect(10, 10, 321, 41))
        font = QtGui.QFont()
        font.setPointSize(20)
        font.setBold(True)
        font.setWeight(75)
        self.label_3.setFont(font)
        self.label_3.setObjectName(_fromUtf8("label_3"))
        self.groupBox = QtGui.QGroupBox(self.centralwidget)
        self.groupBox.setGeometry(QtCore.QRect(20, 50, 281, 191))
        self.groupBox.setObjectName(_fromUtf8("groupBox"))
        self.gridLayoutWidget_2 = QtGui.QWidget(self.groupBox)
        self.gridLayoutWidget_2.setGeometry(QtCore.QRect(40, 110, 201, 71))
        self.gridLayoutWidget_2.setObjectName(_fromUtf8("gridLayoutWidget_2"))
        self.gridLayout_2 = QtGui.QGridLayout(self.gridLayoutWidget_2)
        self.gridLayout_2.setMargin(0)
        self.gridLayout_2.setObjectName(_fromUtf8("gridLayout_2"))
        self.ui_AddressField = QtGui.QLineEdit(self.gridLayoutWidget_2)
        self.ui_AddressField.setObjectName(_fromUtf8("ui_AddressField"))
        self.gridLayout_2.addWidget(self.ui_AddressField, 0, 0, 1, 1)
        self.label = QtGui.QLabel(self.gridLayoutWidget_2)
        self.label.setObjectName(_fromUtf8("label"))
        self.gridLayout_2.addWidget(self.label, 0, 1, 1, 1)
        self.gridLayoutWidget = QtGui.QWidget(self.groupBox)
        self.gridLayoutWidget.setGeometry(QtCore.QRect(30, 30, 210, 71))
        self.gridLayoutWidget.setObjectName(_fromUtf8("gridLayoutWidget"))
        self.gridLayout = QtGui.QGridLayout(self.gridLayoutWidget)
        self.gridLayout.setMargin(0)
        self.gridLayout.setObjectName(_fromUtf8("gridLayout"))
        self.ui_TopOutletLABEL = QtGui.QLabel(self.gridLayoutWidget)
        self.ui_TopOutletLABEL.setObjectName(_fromUtf8("ui_TopOutletLABEL"))
        self.gridLayout.addWidget(self.ui_TopOutletLABEL, 0, 1, 1, 1)
        self.ui_BottomOutletLABEL = QtGui.QLabel(self.gridLayoutWidget)
        self.ui_BottomOutletLABEL.setObjectName(_fromUtf8("ui_BottomOutletLABEL"))
        self.gridLayout.addWidget(self.ui_BottomOutletLABEL, 1, 1, 1, 1)
        self.ui_TopLightBUT = LightButton(self.gridLayoutWidget)
        self.ui_TopLightBUT.setObjectName(_fromUtf8("ui_TopLightBUT"))
        self.gridLayout.addWidget(self.ui_TopLightBUT, 0, 0, 1, 1)
        self.ui_BotLightBUT = LightButton(self.gridLayoutWidget)
        self.ui_BotLightBUT.setObjectName(_fromUtf8("ui_BotLightBUT"))
        self.gridLayout.addWidget(self.ui_BotLightBUT, 1, 0, 1, 1)
        self.ui_TopBUT = QtGui.QPushButton(self.gridLayoutWidget)
        self.ui_TopBUT.setObjectName(_fromUtf8("ui_TopBUT"))
        self.gridLayout.addWidget(self.ui_TopBUT, 0, 2, 1, 1)
        self.ui_BotBut = QtGui.QPushButton(self.gridLayoutWidget)
        self.ui_BotBut.setObjectName(_fromUtf8("ui_BotBut"))
        self.gridLayout.addWidget(self.ui_BotBut, 1, 2, 1, 1)
        self.groupBox_2 = QtGui.QGroupBox(self.centralwidget)
        self.groupBox_2.setGeometry(QtCore.QRect(20, 250, 281, 171))
        self.groupBox_2.setObjectName(_fromUtf8("groupBox_2"))
        self.gridLayoutWidget_3 = QtGui.QWidget(self.groupBox_2)
        self.gridLayoutWidget_3.setGeometry(QtCore.QRect(10, 40, 261, 80))
        self.gridLayoutWidget_3.setObjectName(_fromUtf8("gridLayoutWidget_3"))
        self.gridLayout_3 = QtGui.QGridLayout(self.gridLayoutWidget_3)
        self.gridLayout_3.setMargin(0)
        self.gridLayout_3.setObjectName(_fromUtf8("gridLayout_3"))
        self.ui_ComField = QtGui.QLineEdit(self.gridLayoutWidget_3)
        self.ui_ComField.setObjectName(_fromUtf8("ui_ComField"))
        self.gridLayout_3.addWidget(self.ui_ComField, 0, 1, 1, 1)
        self.ui_ConnectBUT = QtGui.QPushButton(self.gridLayoutWidget_3)
        self.ui_ConnectBUT.setObjectName(_fromUtf8("ui_ConnectBUT"))
        self.gridLayout_3.addWidget(self.ui_ConnectBUT, 0, 2, 1, 1)
        self.ui_DisconnectBUT = QtGui.QPushButton(self.gridLayoutWidget_3)
        self.ui_DisconnectBUT.setObjectName(_fromUtf8("ui_DisconnectBUT"))
        self.gridLayout_3.addWidget(self.ui_DisconnectBUT, 1, 2, 1, 1)
        self.label_2 = QtGui.QLabel(self.gridLayoutWidget_3)
        self.label_2.setObjectName(_fromUtf8("label_2"))
        self.gridLayout_3.addWidget(self.label_2, 0, 0, 1, 1)
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtGui.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 327, 26))
        self.menubar.setObjectName(_fromUtf8("menubar"))
        self.menuFile = QtGui.QMenu(self.menubar)
        self.menuFile.setObjectName(_fromUtf8("menuFile"))
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtGui.QStatusBar(MainWindow)
        self.statusbar.setObjectName(_fromUtf8("statusbar"))
        MainWindow.setStatusBar(self.statusbar)
        self.actionExit = QtGui.QAction(MainWindow)
        self.actionExit.setObjectName(_fromUtf8("actionExit"))
        self.menuFile.addAction(self.actionExit)
        self.menubar.addAction(self.menuFile.menuAction())

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow", None))
        self.label_3.setText(_translate("MainWindow", "ORA Test Program", None))
        self.groupBox.setTitle(_translate("MainWindow", "Outlet Controls", None))
        self.ui_AddressField.setText(_translate("MainWindow", "1", None))
        self.label.setText(_translate("MainWindow", "Target Device Address", None))
        self.ui_TopOutletLABEL.setText(_translate("MainWindow", "Top Outlet", None))
        self.ui_BottomOutletLABEL.setText(_translate("MainWindow", "Bottom Outlet", None))
        self.ui_TopBUT.setText(_translate("MainWindow", "Top", None))
        self.ui_BotBut.setText(_translate("MainWindow", "Bottom", None))
        self.groupBox_2.setTitle(_translate("MainWindow", "Serial Controls", None))
        self.ui_ComField.setText(_translate("MainWindow", "COM16", None))
        self.ui_ConnectBUT.setText(_translate("MainWindow", "Connect", None))
        self.ui_DisconnectBUT.setText(_translate("MainWindow", "Disconnect", None))
        self.label_2.setText(_translate("MainWindow", "COM Port", None))
        self.menuFile.setTitle(_translate("MainWindow", "File", None))
        self.actionExit.setText(_translate("MainWindow", "Exit", None))

from tbx.gui.widgets.lightbutton import LightButton

if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    MainWindow = QtGui.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())


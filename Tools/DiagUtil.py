__author__ = 'rkalb'

import sys
sys.path.append('/ui')

# To Compile UI
# python "C:\Python27\Lib\site-packages\PyQt4\uic\pyuic.py" serial.ui -o uiSerial.py -x

# Insteresting Site
# http://www.jrsoftware.org/isinfo.php
# Need to Get Py2EXE to compile better

import serial
from PyQt4 import QtCore, QtGui
from uiSerial import Ui_MainWindow
import binascii
import socket
import json
import sys, getopt

import DumpDiag

global ser,num,serportnum,comports

class MyForm(QtGui.QMainWindow):
    def __init__(self, comport='COM9', send_port='28001', recv_port='28000', autoConnect=False):
        QtGui.QWidget.__init__(self)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)

        self.NO_POWER = True

        self.UDP_IP = "127.0.0.1"
        self.UDP_RECIEVE_PORT = int(recv_port)
        self.UDP_SEND_PORT = int(send_port)
        self.UDP_MESSGAE = "Welcome to ORA Energy Systems\n"

        self.socketLoc = 1
        self.RSSILoc = 1

        self.pmData = {}

        self.topCurGain = 73.4
        self.topVoltGain = 401
        self.botCurGain = 73.4
        self.botVoltGain = 401

        self.initUI()
        self.show()
        self.ser_port = serial.Serial()
        MemDump = DumpDiag.MemDumpDiag()
        self.lookupTable = MemDump.headerDict
        self.init_send_socket()
        self.init_recieve_socket()

        # Set Default Value on UI
        self.defaultPort = comport
        self.ui.uiComField.setText(self.defaultPort)

        if autoConnect is True:
            self.ui.uiConnectBUT.click()


    def initUI(self):

                # Init Timer
        self.timer = QtCore.QTimer()
        self.timer.setInterval(10)
        #self.timer.timeout.connect(self.timerInterval)
        self.timer.timeout.connect(self.timerInterval)

        self.timer2 = QtCore.QTimer()
        self.timer2.setInterval(1000)
        self.timer2.timeout.connect(self.sendUDP)


        self.ui.uiConnectBUT.clicked.connect(self.init_serial)
        self.ui.uiDisconnectBUT.clicked.connect(self.close_port)
        self.ui.uiTopBUT.clicked.connect(self.TopToggle)
        self.ui.uiBotBut.clicked.connect(self.BotToggle)
        self.ui.actionBasic.triggered.connect(self.stealthMode)
        self.ui.actionAdvanced.triggered.connect(self.viewAll)
        self.ui.actionExit.triggered.connect(sys.exit)

    def init_send_socket(self):
        try:
            self.sendSock = socket.socket(socket.AF_INET,
                                      socket.SOCK_DGRAM)
        except socket.error, msg:
            print 'Failed to create Send socket. Error Code : ' + str(msg[0]) + ' Message ' + msg[1]
        #self.sock.bind((self.UDP_IP, self.UDP_PORT))
        self.sendSock.sendto(self.UDP_MESSGAE, (self.UDP_IP, self.UDP_SEND_PORT))

    def init_recieve_socket(self):
        try:
            self.recSock = socket.socket(socket.AF_INET,
                                  socket.SOCK_DGRAM)
        except socket.error, msg:
            print 'Failed to create Recieve socket. Error Code : ' + str(msg[0]) + ' Message ' + msg[1]

        try:
            self.recSock.bind((self.UDP_IP, self.UDP_RECIEVE_PORT))
            self.recSock.setblocking(0)
        except socket.error, msg:
            print 'Bind failed. Error Code : ' + str(msg[0]) + ' Message ' + msg[1]


    def init_serial(self):
        print "Initing Serial Connection"
        self.comport = str(self.ui.uiComField.text())

        self.ser_port = serial.Serial(self.comport,baudrate=38400, timeout=0)

        if self.ser_port.isOpen():
            print "Port Open"
            self.timer.start()
            self.timer2.start()

    def sendDataSocket(self, message):
        self.sendSock.sendto(message, (self.UDP_IP, self.UDP_SEND_PORT))

    def sendUDP(self):
        self.sendDataSocket(json.dumps(self.pmData))

    def close_port(self):
        print "Closing Port"
        self.ser_port.close()
        self.timer.stop()

    def TopToggle(self):
        print "Toggling Top Outlet"
        if self.ser_port.isOpen():
            data = str(self.ui.uiAddressField.text()) + ":C:SOCKET:TOP:TOGGLE:"
            # print data
            self.ser_port.write(data)
        else:
            print "Serial Port Not Open"

    def BotToggle(self):
        print "Toggling Bottom Outlet"
        if self.ser_port.isOpen():
            data = str(self.ui.uiAddressField.text()) + ":C:SOCKET:BOT:TOGGLE:"
            # print data
            self.ser_port.write(data)
        else:
            print "Serial Port Not Open"
            
    def toggle(self, socket=None):
        print "Toggling Outlet"
        if self.ser_port.isOpen():
            data = str(self.ui.uiAddressField.text()) + ":C:SOCKET:"+socket+":TOGGLE:"
            # print data
            self.ser_port.write(data)
        else:
            print "Serial Port Not Open"

    def readPort(self):
        wait = self.ser_port.inWaiting()
        if wait > 0:
            self.dataIn = self.ser_port.readline()
            print self.dataIn
            self.parseData(self.dataIn)

    def readUDP(self):
        try:
            data = None
            data, addr = self.recSock.recvfrom(1024) # buffer size is 1024 bytes
        except socket.error, msg:
            #print "Socket Recieve Error[" + str(msg[0]) + "]: " + msg[1]
            return
        print "received message:", data
        self.parseUDP(data)

    def timerInterval(self):
        self.readPort()
        self.readUDP()

    def parseUDP(self,data=None):
        #Format 1:(TOP/BOT):(ON/OFF)
        splitData = data.strip("\n\r").split(":")
        if self.pmData.has_key(int(splitData[0])) is False:
            print "Print Address is Not Available\n\r"
            return
        
        if self.pmData[int(splitData[0])][splitData[1]]['State'] == "ON":
            if splitData[2] == "OFF":
                self.toggle(splitData[1])
        if self.pmData[int(splitData[0])][splitData[1]]['State'] == "OFF":
            if splitData[2] == "ON":
                self.toggle(splitData[1])            
        
    def parseData(self,dataIn = None):
        try:
            splitData = dataIn.strip("\n\r").split(':')
            if self.pmData.has_key(int(splitData[0])) is False:
                self.pmData[int(splitData[0])] = {}
                self.pmData[int(splitData[0])]['TOP'] = {}
                self.pmData[int(splitData[0])]['BOT'] = {}
                self.pmData[int(splitData[0])]['RSSI'] = 0
            if splitData[1] == "TOP":
                if splitData[2] == "ON":
                    self.ui.uiTopStatus.setText("ON")
                    self.pmData[int(splitData[0])]['TOP']['State'] = 'ON'
                elif splitData[2] == "OFF":
                    self.ui.uiTopStatus.setText("OFF")
                    self.pmData[int(splitData[0])]['TOP']['State'] = 'OFF'
                elif splitData[2] == "POWER" or splitData[2] == "AMP" or splitData[2] == "VOLT":
                    binNum = bin(int(splitData[3][:-2], 16))[2:]
                    backwards = binNum[::-1]

                    voltNum = 0.00
                    i = -24
                    for bit in backwards:
                        voltNum += ((2 ** i) * int(bit))
                        i+=1

                    if splitData[2] == "AMP":
                        amps = voltNum * self.topCurGain
                        self.ui.ui_TopCurrent.setText(str(amps))
                        self.pmData[int(splitData[0])]['TOP']['CURRENT'] = amps
                    elif splitData[2] == "VOLT":
                        volts = voltNum * self.topVoltGain
                        self.ui.ui_TopVolts.setText(str(volts))
                        self.pmData[int(splitData[0])]['TOP']['VOLTAGE'] = volts
                    elif splitData[2] == "POWER":
                        powers = float(self.ui.ui_TopVolts.text()) * float(self.ui.ui_TopCurrent.text())

                        self.ui.ui_TopPower.setText(str(powers))
                        self.pmData[int(splitData[0])]['TOP']['POWER'] = powers
                    else:
                        print("error")

                    powers = float(self.ui.ui_TopVolts.text()) * float(self.ui.ui_TopCurrent.text())

                    self.ui.ui_TopPower.setText(str(powers))
                    self.pmData[int(splitData[0])]['TOP']['POWER'] = powers


            elif splitData[1] == "BOT":
                if splitData[2] == "ON":
                    self.ui.uiBotStatus.setText("ON")
                    self.pmData[int(splitData[0])]['BOT']['State'] = 'ON'
                elif splitData[2] == "OFF":
                    self.ui.uiBotStatus.setText("OFF")
                    self.pmData[int(splitData[0])]['BOT']['State'] = 'OFF'
                elif splitData[2] == "POWER" or splitData[2] == "AMP" or splitData[2] == "VOLT":
                    binNum = bin(int(splitData[3][:-2], 16))[2:]
                    backwards = binNum[::-1]

                    voltNum = 0.00
                    i = -24
                    for bit in backwards:
                        voltNum += ((2 ** i) * int(bit))
                        i+=1

                    if splitData[2] == "AMP":
                        amps = voltNum * self.botCurGain
                        self.ui.ui_BotCurrent.setText(str(amps))
                        self.pmData[int(splitData[0])]['BOT']['CURRENT'] = amps
                    elif splitData[2] == "VOLT":
                        volts = voltNum * self.botVoltGain
                        self.ui.ui_BotVolts.setText(str(volts))
                        self.pmData[int(splitData[0])]['BOT']['VOLTAGE'] = volts
                    elif splitData[2] == "POWER":
                        powers = float(self.ui.ui_BotVolts.text()) * float(self.ui.ui_BotCurrent.text())

                        self.ui.ui_BotPower.setText(str(powers))
                        self.pmData[int(splitData[0])]['BOT']['POWER'] = powers
                    else:
                        print("error")

                    powers = float(self.ui.ui_BotVolts.text()) * float(self.ui.ui_BotCurrent.text())

                    self.ui.ui_BotPower.setText(str(powers))
                    self.pmData[int(splitData[0])]['BOT']['POWER'] = powers

            elif splitData[1] == "RSSI":
                self.rssiVal = int(splitData[2])
                self.ui.uiRSSILabel.setText(str(self.rssiVal))
                self.pmData[int(splitData[0])]['RSSI'] = int(splitData[2])


            #raise DumpDiag.ParseError("Error parsing data in serial connection")
        except DumpDiag.ParseError as e:
            print e.value
        except IndexError: None
            #print "Index Error on Parser"
        except ValueError: None
            #print "Value Error on Parser"



    def stealthMode(self):
        #self.ui.groupBox_2.setVisible(False)
        self.ui.uiAddressField.setVisible(False)
        self.ui.label.setVisible(False)
        self.ui.uiComField.setVisible(False)
        #self.setGeometry(455,472)

    def viewAll(self):
        #self.ui.groupBox_2.setVisible(True)
        self.ui.uiAddressField.setVisible(True)
        self.ui.label.setVisible(True)
        self.ui.uiComField.setVisible(True)

if __name__ == "__main__":
    comport = 'COM9'
    send_port = '28001'
    recv_port = '28000'
    autoConnect = False
    try:
        opts, args = getopt.getopt(sys.argv[1:],"c:s:r:a",["com=","send=","recv=","help"])
    except getopt.GetoptError:
        print 'DumpUtil.py [-c <"COMPORT"> -s <sending_port> -r <recieving_port> -a "Connects Automatically"]'
        sys.exit(2)

    for opt, arg in opts:
        if opt == ("-c", "--com="):
            comport = arg
            print 'COMPORT: ',arg
        elif opt == ("-s", "--send="):
            send_port = arg
            print 'Sending Port: ',arg
        elif opt == ("-r", "--recv="):
            recv_port = arg
            print 'Recieving Port: ',arg
        elif opt == ("-a"):
            autoConnect = True
            print 'AutoConnecting to Port'
        elif opt == ("--help"):
             print 'DumpUtil.py [-c <"COMPORT"> -s <sending_port> -r <recieving_port> -a "Connects Automatically"]'
    app = QtGui.QApplication(sys.argv)
    myapp = MyForm(comport, send_port, recv_port, autoConnect)
    sys.exit(app.exec_())
    myapp.close_port()
    myapp.timer.stop()
    myapp.timer2.stop()



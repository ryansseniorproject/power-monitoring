//CS5480 Header File

#ifndef CS5480_H
#define CS5480_H

#include "dev_outlet.h"

/* Function Prototypes */
void sendCommand(outlet curOutlet, const char command);
void readRegister(outlet curOutlet, unsigned char* result, int page, int address);
unsigned int writeRegister(outlet curOutlet, int page, int address, unsigned char* data);
int memdump (outlet curOutlet);
void printResult(unsigned char* result, int page, int address);
void printRegister(int page, int address);

/* Definitions */

#define RECIEVE_BYTE 0xFF
#define READ_SIZE    4
#define GAIN50X		0b10
#define GAIN10X		0b00

/***DataSheet***/

//Page 0,Address 0
#define IZX_CH		1
#define NO_OSC		2
#define I1PGA__2	4
#define I2PGA__2	6
#define INT_POL		8

//Page 0,Address 1
#define EPG3_ON		22
#define EPG2_ON		21
#define EPG1_ON		20
#define DO3_OD		18
#define DO2_OD		17
#define DO1_OD		16
#define DO3MODE__4	8
#define DO2MODE__4	4
#define DO1MODE__4	0

//Page 0,Address 5
#define CPCC2		22
#define CPCC1		20
#define FPCC2		9
#define FPCC1		0

//Page 0,Address 7
#define RX_PU_OFF	18
#define RX_CSUM_OFF	17
#define BR__16		0

//Page 0,Address 8
#define FREQ_RNG__4	16
#define PW__16		0

//Page 0,Address 9
#define EPG3IN__4	8
#define EPG2IN__4	4
#define EPG1IN__4	0

//Page 0,Address 34
#define DSP_LCK__5		8
#define HOST_LOCK__5	0

//Page 0,Address 36
#define V1_PEAK__24		0

//Page 0,Address 37
#define I1_PEAK__24		0

//Page 0,Address 38
#define V2_PEAK__24		0

//Page 0,Address 39
#define I2_PEAK__24		0

//Page 0,Address 48
#define DONE		23
#define PSCNT__7	16
#define DIR			5
#define CODE__5		0

#ifdef NON_PARSER
//Page 0,Address 3
#define DRDY		23
#define CRDY		22
#define WOF			21
#define MIPS		18
#define V2SWELL		17
#define V1SWELL		16
#define P2OR		15
#define P1OR		14
#define I2OR		13
#define I1OR		12
#define V2OR		11
#define V1OR		10
#define I2OC		9
#define I1OC		8
#define V2SAG		7
#define V1SAG		6
#define TUP			5
#define FUP			4
#define IC			3
#define RX_CSUM_ERR	2
#define RX_TO		0
#endif

//Page 0,Address 23
#define DRDY		23
#define CRDY		22
#define WOF			21
#define MIPS		18
#define V2SWELL		17
#define V1SWELL		16
#define P2OR		15
#define P1OR		14
#define I2OR		13
#define I1OR		12
#define V2OR		11
#define V1OR		10
#define I2OC		9
#define I1OC		8
#define V2SAG		7
#define V1SAG		6
#define TUP			5	
#define FUP			4
#define IC			3
#define RX_CSUM_ERR	2
#define RX_TO		0

//Page 0,Address 24
#define LCOM__8		8
#define TOD			3
#define VOD			2
#define I2OD		1
#define I1OD		0

//Page 0,Address 25
#define QSUM_SIGN	5
#define Q2_SIGN		4
#define Q1_SIGN		3
#define PSUM_SIGN	2
#define P2_SIGN		1
#define P1_SIGN		0

//Page 0,Address 55
#define ZX_NUM__24	0	

//Page 16,Address 0
#define VFIX		23
#define POS			22
#define ICHAN		21
#define IHOLD		20
#define IVSP		19
#define MCFG__2		17
#define APCM		14
#define ZX_LPF		12
#define AVG_MODE	11
#define REG_CSUM_OFF	10
#define AFC			9
#define I2FLT__2	7
#define V2FLT__2	5
#define I1FLT__2	3
#define V1FLT__2	1
#define IIR_OFF		0

//Page 16,Address 1
#define CHECKSUMS_CRITICAL__24	0

//Page 16,Address 2
#define I1_INST__24			0

//Page 16,Address 3
#define V1_INST__24			0

//Page 16,Address 4
#define P1_INST__24			0

//Page 16,Address 5
#define P1_AVG__24			0

//Page 16,Address 6
#define I1_RMS__24			0

//Page 16,Address 7
#define V1_RMS__24			0

//Page 16,Address 8
#define I2_INST__24			0

//Page 16,Address 9
#define V2_INST__24			0

//Page 16,Address 10
#define P2_INST__24			0

//Page 16,Address 11
#define P2_AVG__24			0

//Page 16,Address 12
#define I2_RMS__24			0

//Page 16,Address 13
#define V2_RMS__24			0

//Page 16,Address 14
#define Q1_AVG__24			0

//Page 16,Address 15
#define Q1_INST__24			0

//Page 16,Address 16
#define Q2_AVG__24			0

//Page 16,Address 17
#define Q2_INST__24			0

//Page 16,Address 20
#define S1__24		0

//Page 16,Address 21
#define PF1__24		0

//Page 16,Address 24
#define S2__24		0

//Page 16,Address 25
#define PF1__24		0

//Page 16,Address 27
#define T__24		0

//Page 16,Address 29
#define P_SUM__24		0		

//Page 16,Address 30
#define S_SUM__24		0

//Page 16,Address 31
#define Q_SUM__24		0

//Page 16,Address 32
#define I1_DCCOFF__24		0

//Page 16,Address 33
#define I1_GAIN__24			0

//Page 16,Address 34
#define V1_DCCOFF__24		0

//Page 16,Address 35
#define V1_GAIN__24			0

//Page 16,Address 36
#define P1_OFF__24			0

//Page 16,Address 37
#define I1_ACOFF__24		0

//Page 16,Address 38
#define Q1_OFF__24			0

//Page 16,Address 39
#define I2_DCCOFF__24		0

//Page 16,Address 40
#define I2_GAIN__24			0

//Page 16,Address 41
#define V2_DCCOFF__24		0

//Page 16,Address 42
#define V2_GAIN__24			0

//Page 16,Address 43
#define P2_OFF__24			0

//Page 16,Address 44
#define I2_ACOFF__24		0

//Page 16,Address 45
#define Q2_OFF__24			0

//Page 16,Address 49
#define LSFR_EPSILON__24	0

//Page 16,Address 50
#define ICHAN_LEVEL__24		0

//Page 16,Address 51
#define SAMPLE_COUNT__24	0

//Page 16,Address 54
#define T_GAIN__24			0

//Page 16,Address 55
#define T_OFF__24			0

//Page 16,Address 56
#define I_RMS_MIN__24		0

//Page 16,Address 57
#define T_SETTLE__24		0

//Page 16,Address 58
#define LOAD_MIN__24		0

//Page 16,Address 59
#define VF_RMS__24			0

//Page 16,Address 60
#define SYS_GAIN__24		0

//Page 16,Address 61
#define SYSTEM_TIME__24		0

//Page 17,Address 0
#define V1SAG_DUR__24		0

//Page 17,Address 1
#define V1SAG_LEVEL__24		0

//Page 17,Address 4
#define I1OVER_DUR__24		0

//Page 17,Address 5
#define I1OVER_LEVEL__24	0

//Page 17,Address 8
#define V2SAG_DUR__24		0

//Page 17,Address 9
#define V2SAG_LEVEL__24		0

//Page 17,Address 12
#define I2OVER_DUR__24		0

//Page 17,Address 13
#define I2OVER_LEVEL__24	0

//Page 18,Address 24
#define IZX_LEVEL__24		0

//Page 18,Address 28
#define ENERGY_PULSE_RATE__24	0

//Page 18,Address 43
#define INT_GAIN__24		0

//Page 18,Address 46
#define V1SWELL_DUR__23		0

//Page 18,Address 47
#define V1SWELL_LEVEL__24	0

//Page 18,Address 50
#define V2SWELL_DUR__23		0

//Page 18,Address 51
#define V2SWELL_LEVEL__24	0

//Page 18,Address 58
#define VZX_LEVEL__24		0

//Page 18,Address 62
#define CYCLE_COUNT__24		0

//Page 18,Address 63
#define SCALE__24			0



//Instructions
extern const char regRead;
extern const char regWrite;
extern const char pageSel;
extern const char instruct;

//Controls
extern const char swReset;
extern const char standby;
extern const char wakeup;
extern const char singConv;
extern const char contConv;
extern const char haltConv;

//Calibrations
extern const char dcOffset;
extern const char acOffset;
extern const char gain;
//Channel
extern const char i1;
extern const char v1;
extern const char i2;
extern const char v2;
const char a4;

#endif //CS540_H
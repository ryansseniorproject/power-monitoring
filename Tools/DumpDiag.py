__author__ = 'rkalb'

# Takes in a Memory dump for CS5480 and Reads back the data in a human Readable format

import os
import sys, getopt, csv

class ParseError(Exception):
    def __init__(self, value):
        self.value = value
    def __str__(self):
        return repr(self.value)


class MemDumpDiag(object):
    def __init__(self, input_file = None, output_file = None, header_file = '..\..\MCU\io\cs5480.h',parent = None):
        self.input_file = input_file
        self.header_file = header_file
        self.output_file = output_file

        #self.inputDict = {}
        self.headerDict = {}

        #self.createInputDict()
        self.createHeaderDict()


    def createInputDict(self):
        pass
        #with open(self.input_file, 'rb') as csvfile:
        #    dataIn = csv.reader(csvfile, delimiter=',')
        #    for row in dataIn:
        #        self.inputDict[(int(row[0]),int(row(1)))] = row[2]

    def createHeaderDict(self):
        with open("./DumpDiag/io_cs5480.h", "r") as fo: #to change directory to update version
            startParse = False
            page, addr = 0,0

            for line in fo:
                if not startParse and line == "/***DataSheet***/\n":
                    startParse = True
                    continue

                if line != "\n" and startParse:
                    if "//Page " in line:
                        splt = line.strip("\n").split(",")
                        if len(splt) != 2:
                            raise ParseError("Error parsing //Page line")
                        else:
                            page = int(splt[0][-2:])
                            addr = int(splt[1][-2:])

                        self.headerDict[(page,addr)] = {}

                    elif "#define " in line:
                        line = line.strip("\n\t#define ")
                        splt = line.replace("\t", "", line.count("\t")-1).split("\t")
                        if len(splt) != 2:
                            raise ParseError("Error parsing #define line")
                        else:
                            if "__" in splt[0]:
                                splt_2 = splt[0].split("__")
                                if len(splt_2) != 2:
                                    raise ParseError("Error parsing #define line: '__' splitting error")
                                else:
                                    reg_name = splt_2[0]
                                    reg_len = int(splt_2[1])
                            else:
                                reg_name = splt[0]
                                reg_len = 0

                                reg_val = int(splt[1])

                            self.headerDict[(page,addr)][reg_name] = (reg_val, reg_len)
                            self.headerDict[(page,addr)][reg_val] = (reg_name, reg_len)

    def processInputFile(self): pass


def main(argv):
    input_file = ''
    output_file = ''
    header_file = ''
    try:
        opts, args = getopt.getopt(argv,"i:h:o:",["ifile=","hfile=","ofile=","help"])
    except getopt.GetoptError:
        print 'DumpDiag.py -i <input file> [-h <header_file -o <output_file>]'
        sys.exit(2)

    for opt, arg in opts:
        if opt == ("-h", "--hfile="):
            header_file = arg
            print 'Header File:',arg
        elif opt == ("-i", "--ifile="):
            input_file = arg
            print 'Input File:',arg
        elif opt == ("-o", "--ofile="):
            output_file = arg
            print 'Output File:',arg
        elif opt == ("--help"):
             print 'DumpDiag.py -i <input file> [-h <header_file -o <output_file>]'



if __name__ == "main":
    main(sys.argv[1:])
